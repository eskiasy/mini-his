from django.shortcuts import render, redirect
# Import a prebuilt login and registration forms
from django.contrib.auth.forms import PasswordChangeForm
from .forms import RegisterForm, EditProfileForm, PatientRegisterForm
from django.contrib import messages
# Create your views here.
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import PasswordChangeView
from django.urls import reverse_lazy
from HIS_app.models import Patient
from django.db import IntegrityError

def register(response):
    if response.method == "POST":
        form = RegisterForm(response.POST)
        patient_reg_form = PatientRegisterForm(response.POST)
        if form.is_valid() and patient_reg_form.is_valid():
            try:
                user = form.save(commit=False)
                user.is_active = True
                user.first_name = patient_reg_form.cleaned_data['First_Name']
                user.last_name = patient_reg_form.cleaned_data['Last_Name']
                user.save()

                new_user = form.cleaned_data['username']
                patient, created = Patient.objects.get_or_create(user=user)
                patient.is_patient = True
                patient.contact_number = patient_reg_form.cleaned_data['contact_number']
                patient.date_of_birth = patient_reg_form.cleaned_data['date_of_birth']
                patient.address = patient_reg_form.cleaned_data['address']
                patient.ec_Name = patient_reg_form.cleaned_data['ec_Name']
                patient.ec_Phone_Number = patient_reg_form.cleaned_data['ec_Phone_Number']
                patient.ec_Address = patient_reg_form.cleaned_data['ec_Address']
                patient.save()
                # username = form.cleaned_data['username'],
                # password = form.cleaned_data['password1']
                # new_user = authenticate(username=form.cleaned_data['username'],
                #                         password=form.cleaned_data['password1'],
                #                         )
                # login(response, new_user)
                messages.success(
                    response, f"Registeration Successful! Login to your Dashboard, Dear {new_user}")
                return redirect("/login")
                # TODO: redirect to profile page
            except IntegrityError as e:
                if "UNIQUE constraint" in str(e) and "contact_number" in str(e):
                    messages.success(
                        response, f"Your Mobile number already exists in our Database. Please try to login or use a different Mobile Number")
                return render(response, "register/register.html", {'form': form, "patient_reg_form":patient_reg_form})
    else:
        form = RegisterForm()
        patient_reg_form = PatientRegisterForm()

    return render(response, "register/register.html", {"form": form, "patient_reg_form":patient_reg_form})


@login_required(login_url='/login/')
def edit_profile(response):
    user_profile = User.objects.get(id=response.user.id)
    form = EditProfileForm(response.POST or None, instance=user_profile)
    if form.is_valid():
        form.save()
        username = form.cleaned_data['username'],
        email = form.cleaned_data['email']
        # update_user = authenticate(username=form.cleaned_data['username'],
        #                         password=form.cleaned_data['password1'],
        #                         )
        # login(response, new_user)
        messages.success(
            response, f"Update Successful!")
        return redirect("/dashboard")
        # TODO: redirect to profile page

    return render(response, "registration/edit_profile.html", {"form": form})


class PasswordsChangeView(PasswordChangeView):
    form_class = PasswordChangeForm
    success_url = reverse_lazy('password_changed')


def password_changed(response):
    messages.success(
        response, f"Password Changed Successfully!")
    return render(response, 'registration/password_changed.html')
