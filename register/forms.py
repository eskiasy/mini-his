from django import forms
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from django.contrib.auth.models import User
from HIS_app.models import Patient
from bootstrap_datepicker_plus.widgets import DatePickerInput, TimePickerInput, DateTimePickerInput, MonthPickerInput, YearPickerInput
from phonenumber_field.formfields import PhoneNumberField



class LoginForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

class RegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

class PatientRegisterForm(forms.ModelForm):
    # contact_number = PhoneNumberField(region="ET")
    # address = forms.CharField(max_length=200)
    First_Name = forms.CharField(max_length=200)
    Last_Name = forms.CharField(max_length=200)
    # ec_Phone_Number = PhoneNumberField(region="ET")
    # ec_Address = forms.CharField(max_length=200)

    class Meta:
        model = Patient
        fields = ['First_Name', 'Last_Name', 'contact_number', 'date_of_birth', 'address', 'ec_Name', 'ec_Phone_Number', 'ec_Address']
        labels = {
            'First_Name': 'First Name',
            'Last_Name': 'Last Name',
            'contact_number': 'Mobile Number',
            'date_of_birth': 'Date of Birth',
            'address': 'Address',
            'ec_Name': 'Emergency Contact Full Name',
            'ec_Phone_Number': 'Emergency Contact Mobile Number',
            'ec_Address': 'Emergency Contact Address'
        }
        widgets = {
            'First_Name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "First Name", 'type': "text", 'aria-label': "Type Your First Name Here", 'aria-describedby': "basic-addon2"}),
            'Last_Name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Last Name", 'type': 'text', 'aria-label': "Type Father's Name Here", 'aria-describedby': "basic-addon2"}),
            'contact_number': forms.TextInput(attrs={'class': "form-control", 'placeholder': "0911223344", 'type': "tel", 'aria-label': "Type Mobile Number Here", 'aria-describedby': "basic-addon2"}),
            'date_of_birth': DatePickerInput(attrs={"class": "input-group date", 'placeholder': "1989-08-12"}, options={
                "format": "YYYY-MM-DD",
                "showTodayButton": True}),
            'address': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Bole, Addis Ababa, Ethiopia", 'type': "text", 'aria-label': "Type Your Home Address Here", 'aria-describedby': "basic-addon2"}),
            'ec_Name': forms.TextInput(attrs={'class': "form-control", 'type': "text", 'aria-label': "Type Emergency Contact Full Name Here", 'aria-describedby': "basic-addon2"}),
            'ec_Phone_Number': forms.TextInput(attrs={'class': "form-control", 'placeholder': "0911223344", 'tel': "text", 'aria-label': "Type Emergency Contact Full Mobile Number", 'aria-describedby': "basic-addon2"}),
            'ec_Address': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Bole, Addis Ababa, Ethiopia", 'type': "text", 'aria-label': "Type Emergency Contact Address Here", 'aria-describedby': "basic-addon2"})
        }


class EditProfileForm(UserChangeForm):
    username = forms.CharField(required=False)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']
        labels = {
            'username': 'User Name',
            'first_name': 'First Name',
            'last_name': 'Last Name',
            'email': 'Email',
        }

        widgets = {
            'email': forms.TextInput(
                attrs={'class': "form-control", 'type': "email", 'placeholder': "email", 'aria-label': "Email", 'aria-describedby': "basic-addon2", "font-size": "10px"}),
            'first_name': forms.TextInput(
                attrs={'class': "form-control", 'type': "text", 'placeholder': "first_name", 'aria-label': "First Name", 'aria-describedby': "basic-addon2", "font-size": "10px"}),
            'last_name': forms.TextInput(
                attrs={'class': "form-control", 'type': "text", 'placeholder': "last_name", 'aria-label': "Last Name", 'aria-describedby': "basic-addon2", "font-size": "10px"}),
            'username': forms.TextInput(
                attrs={'class': "form-control", 'type': "text", 'placeholder': "username", 'aria-label': "User Name", 'aria-describedby': "basic-addon2", "font-size": "10px"}),
        }
