
# Mini-HIS(Health Information System)


A comprehensive system used to manage health-related data and information within healthcare organizations in Ethiopia.

This Project endeavors to mitigate the inherent limitations of the prevailing paper and file-based system employed by health organizations in Ethiopia. In an effort to introduce efficiency and alleviate labor-intensive practices, this project strives to incorporate technologically advanced solutions that capitalize on the data management capabilities of computers.


## Screenshots

![Admin Dashboard](https://gitlab.com/eskiasy/mini-his/-/blob/master/screenshots/admin_dash.png)
![Patient Dashboard](https://gitlab.com/eskiasy/mini-his/-/blob/master/screenshots/patient_dash.png)


## Tech Stack

**FrontEnd:** HTML, CSS, Vanilla JavaScript, Bootstrap

**BackEnd:** Django, Python

**Database:** SQLite3, MySQL


## Color Reference

| Color             | Hex                                                                |
| ----------------- | ------------------------------------------------------------------ |
| Green | ![#589294](https://via.placeholder.com/10/589294?text=+) #589294 |
| Grey | ![#222428](https://via.placeholder.com/10/222428?text=+) #222428 |


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/eskiasy/mini-his.git
```

Go to the project directory

```bash
  cd mini-his
```

Install dependencies

```bash
  pip3 install -r requirements.txt
```

Start the server

```bash
  python3 manage.py runserver
```

## Features

- Appointment Management and Booking
- Patient Management
- Clinic Staff and Admin management
- Dashbords for Admins, Patients, Doctors
- Patient Search By name or phone number


## Authors

- [@EskiasYilma](https://www.github.com/EskiasYilma)

