# Generated by Django 4.2.1 on 2023-06-22 17:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('HIS_app', '0026_alter_appointment_appointment_date_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='doctor',
            name='address',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
