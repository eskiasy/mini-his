# Generated by Django 4.2.1 on 2023-06-22 16:40

import datetime
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('HIS_app', '0025_alter_patient_contact_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='appointment_date',
            field=models.DateField(validators=[django.core.validators.MinValueValidator(limit_value=datetime.date.today)]),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='appointment_time',
            field=models.TimeField(unique=True),
        ),
    ]
