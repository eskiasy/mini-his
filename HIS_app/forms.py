from django import forms
from .models import Patient, Doctor, Appointment, MedicalHistory, Specialization
from phonenumber_field.formfields import PhoneNumberField
from bootstrap_datepicker_plus.widgets import DatePickerInput, TimePickerInput, DateTimePickerInput, MonthPickerInput, YearPickerInput
from django.contrib.auth.models import User

class BookAppointmentForm(forms.Form):

    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': "First Name*", 'type': "text", 'aria-label': "full_name", 'aria-describedby': "basic-addon2", "font-size": "12px"}), required=True)
    last_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': "Last Name*", 'type': "text", 'aria-label': "full_name", 'aria-describedby': "basic-addon2", "font-size": "12px"}), required=True)
    email = forms.EmailField(widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': "Email Address (john@example.com)*", 'type': "email", 'aria-label': "email", 'aria-describedby': "basic-addon2", "font-size": "12px"}), required=False)
    number = PhoneNumberField(region="ET", widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': "Mobile Number (eg. 0912345678, 0116262728)*", 'type': "tel", 'aria-label': "phone", 'aria-describedby': "basic-addon2", "font-size": "12px"}), required=True)
    specialization = forms.ModelChoiceField(queryset=Specialization.objects.all(), widget=forms.Select(
        attrs={'class': "form-control", 'placeholder': "Campaign Title", 'label': 'test', 'type': "text", 'aria-label': "Choose Specialization", 'aria-describedby': "basic-addon2"}), label="Specializations", empty_label='Select Specialization')
    message = forms.CharField(widget=forms.Textarea(
        attrs={'class': "form-control", 'placeholder': "Symptoms/Reasons for Booking*", 'type': "text", 'aria-label': "message", 'aria-describedby': "basic-addon2", "font-size": "15px", 'rows': 10, 'cols': 15}), required=True)


class ContactForm(forms.Form):
    full_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': "Name", 'type': "text", 'aria-label': "full_name", 'aria-describedby': "basic-addon2", "font-size": "12px"}), required=True)
    email = forms.EmailField(widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': "Email Address", 'type': "email", 'aria-label': "email", 'aria-describedby': "basic-addon2", "font-size": "12px"}), required=True)
    phone = forms.CharField(widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': "Mobile Number", 'type': "tel", 'aria-label': "phone", 'aria-describedby': "basic-addon2", "font-size": "12px"}), required=False)
    message = forms.CharField(widget=forms.Textarea(
        attrs={'class': "form-control", 'placeholder': "Message", 'type': "text", 'aria-label': "message", 'aria-describedby': "basic-addon2", "font-size": "15px", 'rows': 10, 'cols': 15}), required=True)

class SearchForm(forms.Form):
    search_query = forms.CharField(widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': "Type Patient name or phone number(eg. 9xxxxxxxx, 11xxxxxxx)", 'type': "text", 'aria-label': "message", 'aria-describedby': "basic-addon2", "font-size": "15px"}), required=False)

class CreateNewPatient(forms.Form):
    pass

class CreateNewDoctor(forms.Form):
    pass

# For Admin Only
class UpdateApptForm(forms.ModelForm):
    specialization = forms.ModelChoiceField(queryset=Specialization.objects.all(), widget=forms.Select(
        attrs={'class': "form-control", 'placeholder': "Campaign Title", 'label': 'test', 'type': "text", 'aria-label': "Choose Specialization", 'aria-describedby': "basic-addon2"}), label="Specializations", empty_label='Select Specialization')
    doctor = forms.ModelChoiceField(queryset=Doctor.objects.all(), widget=forms.Select(
        attrs={'class': "form-control", 'placeholder': "Campaign Title", 'label': 'test', 'type': "text", 'aria-label': "Assign a  Doctor", 'aria-describedby': "basic-addon2"}), label="Doctors", empty_label='Assign a Doctor')
    class Meta:
        model = Appointment
        fields = ('patient', 'doctor', 'appointment_date', 'appointment_time','patient_message','specialization', 'is_approved', 'is_met')
        labels = {
            'patient': 'Patient',
            'doctor': 'Doctor',
            'appointment_date': 'AP_date',
            'appointment_time': 'AP_time',
            'patient_message': 'Patient Message',
            'specialization': 'Specialization',
            'is_approved': 'Approved',
            'is_met': 'Met',
        }
        widgets = {
            'patient': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Patient name", 'type': "text", 'aria-label': "Type Patient Name Here", 'aria-describedby': "basic-addon2", 'readonly': 'readonly'}),
            'patient_message': forms.Textarea(
                attrs={'class': "form-control", 'placeholder': "Symptoms/Reasons for Booking*", 'type': "text", 'aria-label': "message", 'aria-describedby': "basic-addon2", "font-size": "15px", 'rows': 10, 'cols': 15}),
            "appointment_date": DatePickerInput(attrs={"class": "input-group date"},
                                          options={
                "format": "YYYY-MM-DD",
                "showTodayButton": True}),
            "appointment_time": TimePickerInput(),

        }


# For Patient Only
class EditApptForm(forms.ModelForm):
    specialization = forms.ModelChoiceField(queryset=Specialization.objects.all(), widget=forms.Select(
        attrs={'class': "form-control", 'placeholder': "Campaign Title", 'label': 'test', 'type': "text", 'aria-label': "Choose Specialization", 'aria-describedby': "basic-addon2"}), label="Specializations", empty_label='Select Specialization')
    class Meta:
        model = Appointment
        fields = ('first_name', 'last_name', 'patient_message','specialization')
        labels = {
            'patient_message': 'Describe Your Symptoms/Message',
            'first_name': 'First Name of Patient',
            'last_name': 'Last Name of Patient',
        }
        widgets = {
            'patient_message': forms.Textarea(
                attrs={'class': "form-control", 'placeholder': "Symptoms/Reasons for Booking*", 'type': "text", 'aria-label': "message", 'aria-describedby': "basic-addon2", "font-size": "15px", 'rows': 10, 'cols': 15}),
            'first_name': forms.TextInput(
                attrs={'class': "form-control", 'placeholder': "First Name*", 'type': "text", 'aria-label': "full_name", 'aria-describedby': "basic-addon2", "font-size": "12px"}),
            'last_name': forms.TextInput(
                attrs={'class': "form-control", 'placeholder': "Last Name*", 'type': "text", 'aria-label': "full_name", 'aria-describedby': "basic-addon2", "font-size": "12px"})
        }



# For Admin Only
class EditPatientForm(forms.ModelForm):
    class Meta:
        model = Patient
        fields = ['user', 'contact_number', 'date_of_birth', 'address', 'ec_Name', 'ec_Phone_Number', 'ec_Address', 'is_approved', 'is_discharged']

    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)


    def __init__(self, *args, **kwargs):
        super(EditPatientForm, self).__init__(*args, **kwargs)
        # Disable the user field.
        self.fields['user'].disabled = True

        # Set initial values for first_name and last_name fields
        if self.instance and self.instance.user:
            user = self.instance.user
            self.initial['first_name'] = user.first_name
            self.initial['last_name'] = user.last_name

    def clean_contact_number(self):
        contact_number = self.cleaned_data['contact_number']
        # TODO: validation logic for contact number goes here
        return contact_number

    def clean_ec_Phone_Number(self):
        ec_phone_number = self.cleaned_data['ec_Phone_Number']
        # TODO: validation logic for emergency contact phone number goes here
        return ec_phone_number

class AddHistoryForm(forms.ModelForm):
    class Meta:
        model = MedicalHistory
        fields = ["height", "weight", "age", "blood_type", "history"]

class EditDoctorForm(forms.ModelForm):
    class Meta:
        model = Doctor
        fields = ['user', 'contact_number', 'address', 'specialization', 'is_doctor']

    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)


    def __init__(self, *args, **kwargs):
        super(EditDoctorForm, self).__init__(*args, **kwargs)
        # Disable the user field.
        self.fields['user'].disabled = True

        # Set initial values for first_name and last_name fields
        if self.instance and self.instance.user:
            user = self.instance.user
            self.initial['first_name'] = user.first_name
            self.initial['last_name'] = user.last_name

    def clean_contact_number(self):
        contact_number = self.cleaned_data['contact_number']
        # TODO: validation logic for contact number goes here
        return contact_number
