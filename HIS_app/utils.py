# TODO: Appointments: {All, pending, not_met, met, cancelled}
# based on the number of cancellations isolate spam and patient credibility in approval
#
from .models import Patient, Doctor, Appointment, MedicalHistory, Specialization

def create_specialization(response):
    specializations = [
        "Cardiology",
        "Dermatology",
        "Endocrinology",
        "Gastroenterology",
        "Hematology",
        "Infectious Disease",
        "Nephrology",
        "Neurology",
        "Obstetrics and Gynecology",
        "Oncology",
        "Ophthalmology",
        "Orthopedics",
        "Otolaryngology (ENT)",
        "Pediatrics",
        "Plastic Surgery",
        "Psychiatry",
        "Pulmonology",
        "Radiology",
        "Rheumatology",
        "Urology"
    ]
    for i in specializations:
        sp = Specialization(sp_name=str(i))
        sp.save()
