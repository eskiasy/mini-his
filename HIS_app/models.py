from django.contrib.auth.models import AbstractUser
from django.db import models
from django.contrib.auth.models import User
from phonenumber_field.modelfields import PhoneNumberField
from datetime import date
from django.core.validators import MinValueValidator
from django.db.models.signals import post_save
from django.dispatch import receiver


"""
Database models declaration
"""

class Specialization(models.Model):
    sp_name = models.CharField(max_length=200, null=True, unique=True)
    def __str__(self):
        return self.sp_name


class Admin(models.Model):
    # Can be changed to Staff to include more staff
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    contact_number = PhoneNumberField(region='ET', null=True, unique=True)
    date_of_birth = models.DateField()
    address = models.CharField(max_length=200)
    is_admin = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        # Set is_staff to True when saving an Admin instance
        if not self.pk and self.is_admin:  # Only for new instances
            self.user.is_staff = True
            self.user.is_superuser = False
        self.user.save()  # Save the related User instance
        super().save(*args, **kwargs)

class Patient(models.Model):
    # Patient model
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    contact_number = PhoneNumberField(region='ET', null=True)
    # unique is false because a parents might register their children
    # TODO: Failsafe
    date_of_birth = models.DateField(null=True)
    address = models.CharField(max_length=200)
    ec_Name = models.CharField(max_length=100, null=True)
    ec_Phone_Number = PhoneNumberField(region='ET', null=True)
    ec_Address = models.CharField(max_length=200, null=True)
    is_patient = models.BooleanField(default=True)
    is_approved = models.BooleanField(default=False)
    has_history = models.BooleanField(default=False)
    is_discharged = models.BooleanField(default=False)
    # next_appointment = models.BooleanField(default=False)

    # def __str__(self):
    #     return self.user.first_name
    # def save(self, *args, **kwargs):
    #     # Set is_staff to True when saving an Admin instance
    #     if not self.pk and self.is_patient:  # Only for new instances
    #         self.user.is_staff = False
    #         self.user.is_superuser = False
    #     self.user.save()  # Save the related User instance
    #     super().save(*args, **kwargs)

    def clean(self):
        # make sure that the patient is approved before any history is entered
        if not self.is_approved and self.has_history:
            raise ValidationError("Medical history cannot be entered for a pending patient.")
    # @receiver(post_save, sender=User)
    # def set_patient_flag(sender, instance, created, **kwargs):
    #     # Make user a patient upon registration
    #     if created:
    #         patient = Patient.objects.create(user=instance, is_patient=True)
    #         patient.save()

class Doctor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    contact_number = PhoneNumberField(region='ET', null=True, unique=True)
    address = models.CharField(max_length=200, null=True)
    specialization = models.ForeignKey(Specialization, on_delete=models.SET_NULL, null=True)
    is_doctor = models.BooleanField(default=True)
    # slots = models.
    def __str__(self):
        return "{} {}".format(self.user.first_name, self.user.last_name)
    def save(self, *args, **kwargs):
        # Set is_staff to True when saving a Doctor instance
        if not self.pk and self.is_doctor:  # Only for new instances
            self.user.is_staff = True
            self.user.is_superuser = False
        self.user.save()  # Save the related User instance
        super().save(*args, **kwargs)

class Appointment(models.Model):
    first_name = models.CharField(max_length=200, null=True)
    last_name = models.CharField(max_length=200, null=True)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, null=True, limit_choices_to={'is_doctor': True})
    created_on = models.DateTimeField(auto_now_add=True, db_index=True, null=True)
    specialization = models.ForeignKey(Specialization, on_delete=models.SET_NULL, null=True)
    appointment_date = models.DateField(validators=[MinValueValidator(limit_value=date.today)], null=True)
    appointment_time = models.TimeField(unique=True, null=True)
    patient_message = models.TextField(null=True)
    is_approved = models.BooleanField(default=False)
    is_met = models.BooleanField(default=False)
    is_request_cancelled = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.is_request_cancelled:
            appointment_id = self.id  # Get the ID of the appointment
            super().save(*args, **kwargs)  # Save the instance first

            # Delete the appointment after saving
            try:
                appointment = Appointment.objects.get(id=appointment_id)
                appointment.delete()
            except Appointment.DoesNotExist:
                pass
        else:
            super().save(*args, **kwargs)

    def clean(self):
        if not self.is_approved and self.is_met:
            raise ValidationError("Appointments must be approved before meeting.")
        if self.is_request_cancelled and (self.is_met or self.is_approved):
            raise ValidationError("Canceled Appointments cant be approved or met.")

class MedicalHistory(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True)
    doctor = models.ForeignKey(Doctor, on_delete=models.SET_NULL, null=True)
    date = models.DateTimeField(auto_now_add=True, db_index=True, null=True)
    height = models.FloatField(null=True)
    weight = models.FloatField(null=True)
    age = models.IntegerField(null=True)
    # gender is on pause for the moment (however male/female/other to start with)
    blood_type = models.CharField(max_length=10, null=True)
    history = models.TextField(default=None)

    def save(self, *args, **kwargs):
        # Maked "has_history" true after first history is recorded
        is_first_history = not MedicalHistory.objects.filter(patient=self.patient).exists()

        if is_first_history:
            self.patient.has_history = True
            self.patient.save()

        super().save(*args, **kwargs)


