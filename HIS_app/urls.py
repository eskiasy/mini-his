from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("dashboard/", views.dashboard, name="dashboard"),
    path("contact_us/", views.contact_us, name="contact_us"),
    path("about_us/", views.about_us, name="about_us"),
    path("doctors/", views.doctors, name="doctors"),
    path("patients/", views.patients, name="patients"),
    path("medical_history/", views.medical_history, name="medical_history"),
    path("doctor/<int:doctor_id>/edit/", views.edit_doctor, name="edit_doctor"),
    path("doctor/<int:doctor_id>/delete/", views.delete_doctor, name="delete_doctor"),
    path("patient/<int:patient_id>/", views.patient_details, name="patient"),
    path("patient/<int:patient_id>/edit/", views.edit_patient, name="edit_patient"),
    path("patient/<int:patient_id>/delete/", views.delete_patient, name="delete_patient"),
    path("patient/<int:patient_id>/approve/", views.approve_patient, name="approve_patient"),
    path("patient_history/<int:patient_id>/", views.patient_history, name="patient_history"),
    path("appointments/", views.appointments, name="appointments"),
    path("appointment/<int:appt_id>/delete/", views.delete_appt, name="delete_appt"),
    path("appointment/<int:appt_id>/cancel/", views.cancel_appt, name="cancel_appt"),
    path("appointment/<int:appt_id>/approve/", views.approve_appt, name="approve_appt"),
    path("appointment/<int:appt_id>/update/", views.update_appt, name="update_appt"),
    path("appointment/<int:appt_id>/edit/", views.edit_appt, name="edit_appt"),
    path("book_appointment/", views.book_appointment, name="book_appointment"),
    path("my_appointments/", views.my_appointments, name="my_appointments"),
    path("my_history/", views.my_history, name="my_history"),
    ]
