from django.shortcuts import render, redirect
import uuid
from django.contrib.auth.decorators import login_required, user_passes_test
from register.forms import *
from .forms import *
from django.contrib import messages
from .models import Patient, Doctor, Appointment, MedicalHistory
from termcolor import colored
from django.db.models import Q, Count
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import get_object_or_404
from django.http import QueryDict
from . import utils
import datetime
# def home(response):
#     """
#     Defines Home page View
#     A simple login page with clinic info (phone/address/email)
#     - Book Appointments
#     - Login (Doctor, Admin, Reception, Patient)
#     - Register (Patient)
#     """
#     login_form = LoginForm
#     return render(response, "registration/login.html", {'form':login_form})

def home(response):
    # Populate database
    # try:
    #     utils.create_specialization(response)
    # except Exception as e:
    #     print(str(e))
    #     pass

    if response.method == "POST":
        appointment_form = BookAppointmentForm(response.POST, auto_id=True)
        if 'appt_submit' in response.POST and appointment_form.is_valid():
            form_data = appointment_form.cleaned_data
            first_name = appointment_form.cleaned_data["first_name"]
            last_name = appointment_form.cleaned_data["last_name"]
            # validate email from DB
            email = appointment_form.cleaned_data["email"]
            # Validate number from DB
            number = appointment_form.cleaned_data["number"]
            message = appointment_form.cleaned_data["message"]
            specialization = appointment_form.cleaned_data["specialization"]

            if response.user.is_authenticated:
                appointment_form = BookAppointmentForm(response.POST, auto_id=True)
                # Get Patient ID
                try:
                    patient = Patient.objects.get(user=response.user)
                    patient_id = patient.id
                except Patient.DoesNotExist:
                    patient_id = None
                if patient_id:
                    appt_patient = patient_id
                    appt_doctor = None
                    appointment_date = None
                    appointment_time = None
                    patient_message = message
                    appt = Appointment(patient=response.user.patient, patient_message=patient_message, specialization=specialization, first_name=first_name, last_name=last_name)
                    appt.save()
                    messages.success(
                        response, "Appointment Booking Successful!")
                    return redirect('/dashboard')
            else:
                try:
                    patient = Patient.objects.get(contact_number=number)
                    print("exists - redirect to login")
                    messages.success(
                        response, "Dear {} Please Login to Book Appointments.".format(patient.user.first_name))
                    return redirect('/login')
                except Exception as e:
                    print("Doesnt Exist - redirect to registration")
                    query_params = QueryDict(mutable=True)
                    query_params.update(form_data)
                    print(colored(str(e), "red"))
                    return redirect('/register/?{}'.format(query_params.urlencode()))
    else:
        appointment_form = BookAppointmentForm()

    return render(response, "HIS_app/home.html", {"appt_form":appointment_form})

def contact_us(response):
    contact_form = ContactForm()
    return render(response, "HIS_app/contact.html", {"form":contact_form})

def about_us(response):
    return render(response, "HIS_app/about.html")

@login_required(login_url='/login/')
def dashboard(response):
    """
    Defines the super admin view
    - Search Patients
        - Patient
            - add/view
            - approve/ reject request for being a patient user
            - delete
            - edit status (approved / rejected)
    - Doctors
        - delete
        - add/view
    - Appointment
        - add/ view requests
        - delete
        - approve/reject requests from patient or doctor
        - assign doctor to patient
        - Edit ststus (met / not met)
    """
    # Search Patients
    # print(hasattr(response.user, 'specialization'))
    if hasattr(response.user, 'patient') and response.user.patient.is_patient:
        return patient_dashboard(response)
    if hasattr(response.user, 'doctor') and response.user.doctor.is_doctor:
        return doctor_dashboard(response)
    if response.user.is_superuser:
        return admin_dashboard(response)
    else:
        messages.success(
            response, "You don't have access to any dashboard. Please Login or Register to get access.")
        return render(response, "HIS_app/home.html")

@staff_member_required(login_url='/login/')
def patient_details(response, patient_id):
    patient = Patient.objects.get(id=patient_id)
    latest_history = None
    try:
        latest_history = MedicalHistory.objects.filter(patient=patient).latest('date')
    except Exception:
        pass
    print(latest_history)
    search_form = SearchForm(response.POST)
    search_results = None
    if search_form.is_valid():
        search_query = search_form.cleaned_data['search_query']
        search_results = search_patients(search_form)
    print(patient.user.first_name)
    return render(response, "HIS_app/patient.html", {"patient":patient, "search_form":search_form, "search_results":search_results, "latest_history":latest_history})


# @staff_member_required(login_url='/login/')
def search_patients(search_form):
    search_query = search_form.cleaned_data['search_query']
    search_results = None
    if search_query != "":
        # TODO: Impliment phone search
        # if search_query.startswith("09"):
        #     search_query = "+251" + search_query[1:]
        # else:
        search_results = Patient.objects.filter(
            Q(contact_number__icontains=search_query) | Q(user__first_name__icontains=search_query.lower())
        )
        print(search_results)
    return search_results

@staff_member_required(login_url='/login/')
def get_stats(request):
    doctors = Doctor.objects.all().count()
    patients = Patient.objects.all().count()
    appointments = Appointment.objects.all().count()
    appts = Appointment.objects.all()
    pending_appointments = Appointment.objects.filter(is_approved = False).count()
    pending_patients = Patient.objects.filter(is_approved=False)

    print(doctors, patients, appointments, pending_appointments)
    return doctors, patients, appointments, pending_appointments, appts, pending_patients
"""
Admin's Dashboard
"""
@user_passes_test(lambda user: user.is_superuser, login_url='/login/')
def admin_dashboard(response):
    """
    Defines the super admin view
    - Search Patients
        - Patient
            - add/view
            - approve/ reject request for being a patient user
            - delete
            - edit status (approved / rejected)
    - Doctors
        - delete
        - add/view
    - Appointment
        - add/ view requests
        - delete
        - approve/reject requests from patient or doctor
        - assign doctor to patient
        - Edit ststus (met / not met)
    """
    print("Admin")
    doctors, patients, appointments, pending_appointments, appts, pending_patients = get_stats(response)
    search_results = None
    search_query = None
    if response.method == "POST":
        search_form = SearchForm(response.POST)
        if search_form.is_valid():
            search_query = search_form.cleaned_data['search_query']
            print(search_query)
            try:
                search_results = search_patients(search_form)
                if str(search_results) == "<QuerySet []>":
                    search_results = None
            except Exception as e:
                print(str(e))
                pass
    else:
        search_form = SearchForm(response.POST)
    return render(response, "HIS_app/dashboard_admin.html", {"search_form":search_form, "search_results":search_results, "doctors":doctors, "patients":patients, "appointments":appointments, "pending_appointments":pending_appointments, "appts":appts, "search_query":search_query, "pending_patients":pending_patients})

"""
Doctors Dashboard
"""
@staff_member_required(login_url='/login/')
def doctor_dashboard(response):
    """
    - Appointments
        - add/ view
        - request cancel for assignments by admin
        - Edit appointment status (met / not met)
    - Patient
        - add/view all patients under doctor id
        - add/edit history for all patients under doctor id
    """
    print("Doctor")
    return render(response, "HIS_app/dashboard_doctor.html")

"""
Patients Dashboard
"""
@login_required(login_url='/login/')
def patient_dashboard(response):
    """
    - Registration/login
        - Register as a patient (should be approved by admin)
        - login as a patient
    - Appointment
        - apply for appt request
        - view appointments
        - View personal history
        - get Notifications
        - cancel request
    """
    patient_stats = response.user.patient.is_approved
    try:
        appointment = Appointment.objects.filter(patient=response.user.patient).order_by('-appointment_date')
        appt_latest = appointment.filter(appointment_date__gt=datetime.date.today()).first()
        appt_past_due = Appointment.objects.filter(appointment_date__lt=datetime.date.today())
    except Exception as e:
        print(colored(str(e)))
        appointment = None
        appt_latest = None
        appt_past_due = None
    if response.method == "POST":
        appointment_form = BookAppointmentForm(response.POST, auto_id=True)
        if 'appt_submit' in response.POST and appointment_form.is_valid():
            form_data = appointment_form.cleaned_data
            first_name = appointment_form.cleaned_data["first_name"]
            last_name = appointment_form.cleaned_data["last_name"]
            # validate email from DB
            email = appointment_form.cleaned_data["email"]
            # Validate number from DB
            number = appointment_form.cleaned_data["number"]
            message = appointment_form.cleaned_data["message"]

            if response.user.is_authenticated:
                appointment_form = BookAppointmentForm(response.POST, auto_id=True)
                # Get Patient ID
                try:
                    patient = Patient.objects.get(user=response.user)
                    patient_id = patient.id
                except Patient.DoesNotExist:
                    patient_id = None
                if patient_id:
                    appt_patient = patient_id
                    appt_doctor = None
                    appointment_date = None
                    appointment_time = None
                    patient_message = message
                    appt = Appointment(patient=response.user.patient, patient_message=patient_message, first_name=first_name, last_name=last_name)
                    appt.save()
                    messages.success(
                        response, "Appointment Booking Successful!")
                    return redirect('/dashboard')

    appointment_form = BookAppointmentForm(response.POST or None, auto_id=True)
    # if response.user.patient.is_approved:
    #     print("Patient")
    return render(response, "HIS_app/dashboard_patient.html", {"patient_stats":patient_stats, "appt_form":appointment_form, "appointment":appointment, "appt_latest":appt_latest, "appt_past_due":appt_past_due})
    # else:
    #     print("Patient_pending")
    #     return render(response, "HIS_app/pending_patient.html")

"""
Doctors ADMIN
"""
@staff_member_required(login_url='/login/')
def doctors(response):
    today = datetime.date.today()
    doctors = Doctor.objects.all().annotate(all_appointment_count=Count('appointment'),
        todays_appointment_count=Count('appointment', filter=Q(appointment__appointment_date=today)))
    appts = Appointment.objects.all()
    # pending_appts = {}
    # for i in appts:
    #     if i.doctor.id in
    return render(response, "HIS_app/doctors.html", {"doctors":doctors})

@staff_member_required(login_url='/login/')
def edit_doctor(response, doctor_id):
    doctor = Doctor.objects.get(id=doctor_id)
    if response.method == "POST":
        form = EditDoctorForm(response.POST, instance=doctor)
        if form.is_valid():
            user = doctor.user
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.save()
            form.save()
            messages.success(
                response, "Doctor Updated Successfully!")
            return redirect(doctors)
    else:
        form = EditDoctorForm(instance=doctor)

    return render(response, "HIS_app/edit_doctor.html", {"doctor":doctor, "form":form})

@staff_member_required(login_url='/login/')
def delete_doctor(response, patient_id):
    doctor = Doctor.objects.get(id=doctor_id).delete()
    return redirect(doctors)


"""
Patients ADMIN
"""
@staff_member_required(login_url='/login/')
def patients(response):
    patients = Patient.objects.all().order_by('-user__date_joined')
    pending_patients = Patient.objects.filter(is_approved=False)
    pending_appointments = Appointment.objects.filter(is_approved = False)
    # pending_appts = {}
    # for i in appts:
    #     if i.doctor.id in
    return render(response, "HIS_app/patients.html", {"patients":patients, "pending_patients":pending_patients, "pending_appointments":pending_appointments})

@staff_member_required(login_url='/login/')
def edit_patient(response, patient_id):
    patient = Patient.objects.get(id=patient_id)
    current_data = Patient.objects.filter(id=patient_id)
    try:
        medical_history = MedicalHistory.objects.filter(patient=patient).first()
    except Exception:
        medical_history = None
        # medical_history.save()
    if response.method == "POST":
        patient_form = EditPatientForm(response.POST, instance=patient)
        history_form = AddHistoryForm(response.POST, instance=medical_history)
        if patient_form.is_valid() and history_form.is_valid():
            # Create the new medical history entry
            medical_history = history_form.save(commit=False)
            medical_history.patient = patient
            medical_history.save()

            user = patient.user
            user.first_name = patient_form.cleaned_data['first_name']
            user.last_name = patient_form.cleaned_data['last_name']
            user.save()
            patient_form.save()
            messages.success(
                response, "Patient Updated Successfully!")
            return redirect('/patient/{}'.format(patient_id))
    else:
        patient_form = EditPatientForm(instance=patient)
        history_form = AddHistoryForm(instance=medical_history)

    return render(response, "HIS_app/edit_patient.html", {"patient":patient, "form":patient_form, "AddHistoryForm":history_form})


@staff_member_required(login_url='/login/')
def delete_patient(response, patient_id):
    patient = Patient.objects.get(id=patient_id).delete()
    return redirect(patients)


@staff_member_required(login_url='/login/')
def approve_patient(response, patient_id):
    patient = Patient.objects.get(id=patient_id)
    patient.is_approved = True
    patient.save()
    return redirect(patients)


@staff_member_required(login_url='/login/')
def update_patient(response, patient_id):
    patient = Patient.objects.get(id=patient_id).delete()
    return redirect(patients)

"""
Appointments # ADMINS
"""
@staff_member_required(login_url='/login/')
def appointments(response):
    appointments = Appointment.objects.all().order_by('-created_on')
    return render(response, "HIS_app/appointments.html", {"appointments":appointments})

@staff_member_required(login_url='/login/')
def delete_appt(response, appt_id):
    appointment = Appointment.objects.get(id=appt_id).delete()
    return redirect(appointments)

@staff_member_required(login_url='/login/')
def approve_appt(response, appt_id):
    appointment = Appointment.objects.get(id=appt_id)
    appointment.is_approved = True
    appointment.save()
    return redirect(appointments)

@staff_member_required(login_url='/login/')
def update_appt(response, appt_id):
    appointment = Appointment.objects.get(id=appt_id)
    print(colored(appointment.patient.is_approved, "green"))
    current_data = Appointment.objects.filter(id=appt_id)
    form = UpdateApptForm(response.POST or None, instance=appointment)
    if appointment.patient.is_approved:
        if form.is_valid():
            appointment.patient_message = form.cleaned_data["patient_message"]
            appointment.doctor = form.cleaned_data['doctor']
            appointment.patient = form.cleaned_data['patient']
            appointment.appointment_date = form.cleaned_data['appointment_date']
            appointment.appointment_time = form.cleaned_data['appointment_time']
            appointment.specialization = form.cleaned_data['specialization']
            try:
                if response.POST['is_approved']:
                    print("Approved")
                    appointment.is_approved = True
                else:
                    appointment.is_approved = False
            except Exception:
                appointment.is_approved = False
            try:
                if response.POST['is_met']:
                    print("Met")
                    appointment.is_met = True
                else:
                    appointment.is_met = False
            except Exception:
                appointment.is_met = False
            appointment.save()
            messages.success(
                response, "Appointment Updated Successfully!")
            return redirect(appointments)
    else:
        messages.success(
            response, "Patient's Account is not Verified! Please Activate account to Proceed.")
        return redirect('/patient/{}/'.format(appointment.patient.id))

    return render(response, "HIS_app/update_appointments.html", {"form":form})

"""
Appointments # PATIENTS
"""

@login_required(login_url='/login/')
def cancel_appt(response, appt_id):
    appointment = Appointment.objects.get(id=appt_id)
    appointment.is_request_cancelled = True
    appointment.save()
    return redirect(my_appointments)

@login_required(login_url='/login/')
def book_appointment(response):
    """
    patients to book appointments
    """
    patient = Patient.objects.get(id=response.user.patient.id)
    if response.method == "POST":
        appointment_form = BookAppointmentForm(response.POST, auto_id=True)
        if 'appt_submit' in response.POST and appointment_form.is_valid():
            form_data = appointment_form.cleaned_data
            first_name = appointment_form.cleaned_data["first_name"]
            last_name = appointment_form.cleaned_data["last_name"]
            # validate email from DB
            email = appointment_form.cleaned_data["email"]
            # Validate number from DB
            number = appointment_form.cleaned_data["number"]
            message = appointment_form.cleaned_data["message"]
            specialization = appointment_form.cleaned_data['specialization']

            if response.user.is_authenticated:
                appointment_form = BookAppointmentForm(response.POST, auto_id=True)
                # Get Patient ID
                try:
                    patient = Patient.objects.get(user=response.user)
                    patient_id = patient.id
                except Patient.DoesNotExist:
                    patient_id = None
                if patient_id:
                    appt_patient = patient_id
                    appt_doctor = None
                    appointment_date = None
                    appointment_time = None
                    patient_message = message
                    appt = Appointment(patient=response.user.patient, patient_message=patient_message, specialization=specialization, first_name=first_name, last_name=last_name)
                    appt.save()
                    messages.success(
                        response, "Appointment Booking Successful!")
                    return redirect('/dashboard')


    appointment_form = BookAppointmentForm(response.POST or None, auto_id=True)

    patient_stats = response.user.patient.is_approved

    return render(response, "HIS_app/book_appointment.html", {"appt_form":appointment_form, "patient_stats":patient_stats})

@login_required(login_url='/login/')
def my_appointments(response):
    appointments = Appointment.objects.filter(patient=response.user.patient).order_by('-created_on')
    print(appointments)
    patient_stats = response.user.patient.is_approved
    return render(response, "HIS_app/my_appointments.html", {"appointments":appointments, "patient_stats":patient_stats})

@login_required(login_url='/login/')
def edit_appt(response, appt_id):
    appointment = Appointment.objects.get(id=appt_id)
    current_data = Appointment.objects.filter(id=appt_id)
    form = EditApptForm(response.POST or None, instance=appointment)
    if form.is_valid():
        appointment.patient_message = form.cleaned_data["patient_message"]
        appointment.specialization = form.cleaned_data['specialization']
        appointment.first_name = form.cleaned_data['first_name']
        appointment.last_name = form.cleaned_data['last_name']
        appointment.save()
        messages.success(
            response, "Appointment Updated Successfully!")
        return redirect(my_appointments)

    return render(response, "HIS_app/edit_appointment.html", {"form":form})


"""
Patient History # PATIENTS
"""

@login_required(login_url='/login/')
def my_history(response):
    patient = Patient.objects.get(id=response.user.patient.id)
    history = MedicalHistory.objects.filter(patient=patient)
    patient_stats = response.user.patient.is_approved
    return render(response, "HIS_app/my_history.html", {"patient":patient, "history":history, "patient_stats":patient_stats})

"""
Patient History # ADMINS
"""
@staff_member_required(login_url='/login/')
def patient_history(response, patient_id):
    patient = Patient.objects.get(id=patient_id)
    history = MedicalHistory.objects.filter(patient=patient)

    return render(response, "HIS_app/patient_history.html", {"patient":patient, "history":history})

@staff_member_required(login_url='/login/')
def medical_history(response):
    history = MedicalHistory.objects.all()

    return render(response, "HIS_app/medical_history.html", {"history":history})
