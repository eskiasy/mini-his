from django.test import TestCase
from faker import Faker

def dummy_text_generator():
    # Generate multiple paragraphs of Lorem Ipsum text
    fake = Faker()

    # Generate a paragraph of Lorem Ipsum text
    lorem_ipsum_paragraph = fake.paragraph()


    num_paragraphs = 15
    lorem_ipsum_paragraphs = [fake.paragraph() for _ in range(num_paragraphs)]
    for i in lorem_ipsum_paragraphs:
        print(i)


if __name__ == "__main__":
    dummy_text_generator()
