from rest_framework import serializers

class HomeSerializer(serializers.Serializer):
    # Define the fields to include in the API response
    status = serializers.CharField()
